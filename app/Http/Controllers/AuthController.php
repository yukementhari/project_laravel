<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pendaftaran(){
        return view('halaman.register');
    }

    public function signup(request $request){
        $nama = $request['fname'];
        $name = $request['lname'];
        return view('halaman.welcome', compact('nama','name'));
        
    }
}
