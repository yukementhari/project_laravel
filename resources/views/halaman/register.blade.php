<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
     @csrf
     <label for="fname">First Name:</label><br>
     <input type="text" id="fname" name="fname"><br><br>
     <label for="lname">Last Name:</label><br>
     <input type="text" id="lname" name="lname"><br><br>
     <label for="gender">Gender:</label><br>
     <input type="radio" id="man" name="gender" value="Man">
     <label for="man">Man</label><br>
     <input type="radio" id="woman" name="gender" value="Woman">
     <label for="woman">Woman</label><br>
     <input type="radio" id="other" name="gender" value="Other">
     <label for="other">Other</label><br><br>
     <label for="nationality">Nationality:</label>
    <select id="nationality" name="nationality">
     <option value="indonesian">Indonesian</option>
     <option value="british">British</option>
     <option value="arabian">Arabian</option>
     <option value="japanese">Japanese</option>
    </select><br><br>
    <label for="language">Language Spoken:</label><br>
     <input type="checkbox" id="language1" name="language1" value="bahasa indonesia">
     <label for="language1"> Bahasa Indonesia </label><br>
     <input type="checkbox" id="language2" name="language2" value="english">
     <label for="language2"> English </label><br>
     <input type="checkbox" id="language3" name="language3" value="arabic">
     <label for="language3"> Arabic </label><br>
     <input type="checkbox" id="language4" name="language4" value="japanese">
     <label for="language4"> Japanese </label><br><br>
     <label for="bio">Bio:</label><br>
     <textarea name="bio" rows="10" cols="30"> </textarea><br><br>
     <input type="submit" value="Sign Up">
</form>
</body>
</html>